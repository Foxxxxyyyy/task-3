import { callApi } from '../helpers/apiHelper';
import { createFighterPreview, reateFighterImage, createFighterImage } from '../components/fighterPreview';
import { fighters } from '../helpers/mockData';
import App from '../app';

class FighterService {
  static posPreview = ["left","right"];

  async getFighters() {
    try {
      const endpoint = 'fighters';
      const apiResult = await callApi(endpoint, 'GET');
      this.selectedFighters = [undefined,undefined];
      this.selectLast = false;
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    try {
      let fighter;
      for (let i=0; i < fighters.length; i++) {
        if (fighters[i]._id == id) {
          fighter = fighters[i];
          break;
        }
      }
      if (fighter) {
        if ((!fighter.health)||(!fighters.attack)||(!fighters.defense)) {
          let url = "./resources/api/details/fighter/"+id+".json";
          let response = await fetch(url);
          if (response.ok) { 
            let params = await response.json();
            fighter.health = params.health;
            fighter.attack = params.attack;
            fighter.defense = params.defense;
          } else {
            alert("Error load details ! ("+id+")");
          }
        }
        let curr = this.selectLast ? 1 : 0;
        this.selectLast = !this.selectLast;
        this.selectedFighters[curr] = fighter;
        createFighterPreview(fighter, FighterService.posPreview[curr]);        
        return fighter;
      } else throw "Error";
    } catch (error) {
      alert("Error search fighter "+id+" !");
    };
  }

  async getSelectedFighters() {
    return Promise.resolve(this.selectedFighters);
  }

}

export const fighterService = new FighterService();
